import cats.{Applicative, Eval, Functor, Traverse}
import cats.data.State
import cats.implicits._

case class RawName(value: String)
case class ResolvedName(value: String)

final case class Column[I, R](info: I, value: R)
object Column {
  implicit def columnInstances[I]: Functor[Column[I, ?]] = new Functor[Column[I, ?]] {
    def map[A, B](fa: Column[I, A])(f: A => B): Column[I, B] = fa.copy(value = f(fa.value))
  }
}

sealed trait Node[I, R]
final case class ColumnWrap[I, R](info: I, col: Column[I, R]) extends Node[I, R]
object ColumnWrap {
  implicit def columnWrapInstances[I]: Traverse[ColumnWrap[I, ?]] = new Traverse[ColumnWrap[I, ?]] {
    override def map[A, B](fa: ColumnWrap[I, A])(f: A => B): ColumnWrap[I, B] = fa.copy(col = fa.col.map(f))
    def foldLeft[A, B](fa: ColumnWrap[I, A], b: B)(f: (B, A) => B): B = f(b, fa.col.value)
    def foldRight[A, B](fa: ColumnWrap[I, A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = Eval.defer(f(fa.col.value, lb))
    def traverse[G[_], A, B](fa: ColumnWrap[I, A])(f: A => G[B])(implicit G: Applicative[G]): G[ColumnWrap[I, B]] =
      Applicative[G].map(f(fa.col.value))(b => fa.map(_ => b))
  }
}

object Main {
  def resolve(n: RawName): State[String, ResolvedName] = State { acc =>
    (acc + s"\nresolved ${n.value}", ResolvedName(n.value))
  }

  def resolveNodeWithoutTraverse[I](node: Node[I, RawName]): State[String, Node[I, ResolvedName]] = State { acc =>
    node match {
      case c: ColumnWrap[I, RawName] => {
        val (nacc, resolvedC) = resolve(c.col.value).run(acc).value
        (nacc, ColumnWrap(c.info, Column(c.col.info, resolvedC)))
      }
    }
  }

  def resolveNode[I](node: Node[I, RawName]): State[String, Node[I, ResolvedName]] =
    node match {
      case c: ColumnWrap[I, RawName] => c.traverse(resolve).widen
    }
}
