package FuncsWithArbitrary

import cats.{Eq, Functor}
import cats.implicits._


final case class Column[I, R](info: I, value: R)
object Column {
  implicit def eqColumn[I: Eq, R: Eq]: Eq[Column[I, R]] = Eq.fromUniversalEquals

  implicit def columnInstances[I]: Functor[Column[I, ?]] = new Functor[Column[I, ?]] {
    def map[A, B](fa: Column[I, A])(f: A => B): Column[I, B] = fa.copy(value = f(fa.value))
  }
}

sealed trait Node[I, R]
object Node {
  implicit def eqNode[I: Eq, R: Eq]: Eq[Node[I, R]] = Eq.fromUniversalEquals

  implicit def treeInstances[I]: Functor[Node[I, ?]] = new Functor[Node[I, ?]] {
    def map[A, B](fa: Node[I, A])(f: A => B): Node[I, B] = fa match {
      case n: Wrap[I, _] => n.map(f)
      case n: ManyCols[I, _] => n.map(f)
      case n: ManyNodes[I, _] => n.map(f)
    }
  }
}

final case class Wrap[I, R](info: I, col: Column[I, R]) extends Node[I, R]
object Wrap {
  implicit def eqWrap[I: Eq, R: Eq]: Eq[Wrap[I, R]] = Eq.fromUniversalEquals

  implicit def columnWrapInstances[I]: Functor[Wrap[I, ?]] = new Functor[Wrap[I, ?]] {
    def map[A, B](fa: Wrap[I, A])(f: A => B): Wrap[I, B] = fa.copy(col = fa.col.map(f))
  }
}

final case class ManyCols[I, R](info: I, cols: List[Column[I, R]]) extends Node[I, R]
object ManyCols {
  implicit def eqManyCols[I: Eq, R: Eq]: Eq[ManyCols[I, R]] = Eq.fromUniversalEquals

  implicit def manyColsInstances[I]: Functor[ManyCols[I, ?]] = new Functor[ManyCols[I, ?]] {
    def map[A, B](fa: ManyCols[I, A])(f: A => B): ManyCols[I, B] = fa.copy(cols = fa.cols.map(_.map(f)))
  }
}

final case class ManyNodes[I, R](info: I, cols: List[Node[I, R]]) extends Node[I, R]
object ManyNodes {
  implicit def eqManyNodes[I: Eq, R: Eq]: Eq[ManyNodes[I, R]] = Eq.fromUniversalEquals

  implicit def manyColsInstances[I]: Functor[ManyNodes[I, ?]] = new Functor[ManyNodes[I, ?]] {
    def map[A, B](fa: ManyNodes[I, A])(f: A => B): ManyNodes[I, B] = fa.copy(cols = fa.cols.map(_.map(f)))
  }
}
