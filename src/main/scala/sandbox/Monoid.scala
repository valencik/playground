package sandbox

object MyMonoid extends App {

  trait Semigroup[A] {
    def combine(x: A, y: A): A
  }

  trait Monoid[A] extends Semigroup[A] {
    def empty: A
  }

  def combineAll[A](as: Seq[A])(implicit m: Monoid[A]): A =
    as.foldLeft(m.empty)(m.combine)

  implicit val intAdditionMonoid: Monoid[Int] = new Monoid[Int] {
    def empty: Int = 0
    def combine(x: Int, y: Int): Int = x + y
  }

  val xs = Seq(10, 20, 30)
  val combined = combineAll(1 +: xs) //61

  println(combined)
}
