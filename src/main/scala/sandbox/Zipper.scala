package sandbox

import cats._
import cats.implicits._

case class ListZipper[A] (ls: List[A], f: A, rs: List[A]) {
  def leftMaybe = this match {
    case ListZipper(Nil, f, rs) => None
    case ListZipper(l::ls, f, rs) => Some(ListZipper(ls, l, f::rs))
  }
  def left = leftMaybe.getOrElse(this)
  def rightMaybe = this match {
    case ListZipper(ls, f, Nil) => None
    case ListZipper(ls, f, r::rs) => Some(ListZipper(f::ls, r, rs))
  }
  def right = rightMaybe.getOrElse(this)
  def toList = ls ++ (f :: rs)
  def lHead = ls.headOption.getOrElse(f)
  def rHead = rs.headOption.getOrElse(f)
  def zipZip[B](oz: ListZipper[B]): ListZipper[(A, B)] =
    ListZipper(ls.zip(oz.ls), (f, oz.f), rs.zip(oz.rs))
}
object ListZipper {
  def apply[A](xs: List[A], fi: Int): ListZipper[A] = {
    val (ls, rs) = xs.splitAt(fi)
    new ListZipper(ls.reverse, rs.head, rs.tail)
  }

  implicit def listZipperShow[A:Show]: Show[ListZipper[A]] =
    new Show[ListZipper[A]] {
      def show(z: ListZipper[A]): String = {
        val ls = z.ls.reverse.mkString(",")
        val rs = z.rs.mkString(",")
        val f =  z.f.show
        s"|$ls|> $f <|$rs|"
      }
  }

  implicit val catsInstancesForListZipper: Comonad[ListZipper] = new Comonad[ListZipper] {
    def map[A, B](za: ListZipper[A])(f: A => B): ListZipper[B] =
      ListZipper(za.ls.map(f), f(za.f), za.rs.map(f))

    def extract[A](z: ListZipper[A]): A = z.f

    override def coflatten[A](za: ListZipper[A]): ListZipper[ListZipper[A]] = {
      val lefts: List[ListZipper[A]] = List.iterate(za.left, za.ls.size)(_.left)
      val rights: List[ListZipper[A]] = List.iterate(za.right, za.rs.size)(_.right)
      ListZipper(lefts, za, rights)
    }

    def coflatMap[A, B](za: ListZipper[A])(f: ListZipper[A] => B): ListZipper[B] = {
      coflatten(za).map(f)
    }
  }
}

object Zipper extends App {
  def latch[A](z: ListZipper[A])(implicit ordering: Ordering[A]): A =
    if (z.ls.isEmpty) z.f else (z.f :: z.ls).max

  def peak[A](z: ListZipper[A])(implicit ordering: Ordering[A]): Boolean =
    ordering.lt(z.lHead, z.f) && ordering.lt(z.rHead, z.f)

  def wma(n: Int)(z: ListZipper[Double]): Double = {
    val window = z.ls.take(n) ++ (z.f :: z.rs.take(n))
    window.sum / window.size
  }

  val iz: ListZipper[Int] = ListZipper(List(1,2,3,2,1,3,4,5,4,2,8), 0)
  print(iz.show)

  val leftMaxes = iz.coflatMap[Int](latch)
  println(leftMaxes.show)

  val peaks = iz.coflatMap(peak(_))
  println(peaks.show)

  val averages = iz.map(_.toDouble).coflatMap(wma(5)).map{d => f"$d%.3f"}
  println(averages.show)

  val ilz = iz.zipZip(leftMaxes).zipZip(peaks).zipZip(averages).map{
    case (((i, l), p), a) => (i, l, p, a)
  }
  print(ilz.map{case (i, l, p, a) => s"$i   $l   $p   $a\n"}.toList.mkString)
}
