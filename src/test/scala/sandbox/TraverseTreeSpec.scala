package TraverseTree

import cats.implicits._
import org.scalatest.funsuite.AnyFunSuite
import org.typelevel.discipline.scalatest.Discipline

import cats.Traverse
import org.scalacheck.{Arbitrary, Gen}
import org.scalacheck.Arbitrary.{arbitrary => getArbitrary}

import cats.laws.discipline.{SerializableTests, TraverseTests}

object arbitrary {
  implicit def arbColumn[I: Arbitrary, R: Arbitrary]: Arbitrary[Column[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[R] } yield Column(i, r))

  implicit def arbRelation[I: Arbitrary, R: Arbitrary]: Arbitrary[Relation[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[R] } yield Relation(i, r))

  implicit def arbColumnWrap[I: Arbitrary, R: Arbitrary]: Arbitrary[ColumnWrap[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- getArbitrary[Column[I, R]] } yield ColumnWrap(i, r))

  implicit def arbManyCols[I: Arbitrary, R: Arbitrary]: Arbitrary[ManyCols[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- Gen.resize(3, getArbitrary[List[Column[I, R]]]) } yield ManyCols(i, r))

  implicit def arbFrom[I: Arbitrary, R: Arbitrary]: Arbitrary[From[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; r <- Gen.resize(3, getArbitrary[List[Relation[I, R]]]) } yield From(i, r))

  implicit def arbQuery[I: Arbitrary, R: Arbitrary]: Arbitrary[Query[I, R]] =
    Arbitrary(for { i <- getArbitrary[I]; s <- getArbitrary[ManyCols[I, R]]; f <- getArbitrary[From[I, R]] } yield Query(i, s, f))
}

class TraverseTreeLawTests extends AnyFunSuite with Discipline {
  import arbitrary._

  checkAll("Column[Int, Int] with Option",
           TraverseTests[Column[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[Column[Int, ?]]", SerializableTests.serializable(Traverse[Column[Int, ?]]))

  checkAll("Relation[Int, Int] with Option",
           TraverseTests[Relation[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[Relation[Int, ?]]", SerializableTests.serializable(Traverse[Relation[Int, ?]]))

  checkAll("ColumnWrap[Int, Int] with Option",
           TraverseTests[ColumnWrap[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[ColumnWrap[Int, ?]]", SerializableTests.serializable(Traverse[ColumnWrap[Int, ?]]))

  checkAll("ManyCols[Int, Int] with Option",
           TraverseTests[ManyCols[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[ManyCols[Int, ?]]", SerializableTests.serializable(Traverse[ManyCols[Int, ?]]))

  checkAll("From[Int, Int] with Option",
           TraverseTests[From[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[From[Int, ?]]", SerializableTests.serializable(Traverse[From[Int, ?]]))

  checkAll("Query[Int, Int] with Option",
           TraverseTests[Query[Int, ?]].traverse[Int, Int, Int, Set[Int], Option, Option])
  checkAll("Traverse[Query[Int, ?]]", SerializableTests.serializable(Traverse[Query[Int, ?]]))


}
