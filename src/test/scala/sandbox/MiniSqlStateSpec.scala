package sandbox

import sandbox.MiniSqlState._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.Matchers._
import org.scalatest.Inside

class MiniSqlStateSpec extends AnyFlatSpec with Inside {

  def initialCatalog: Resolver = Resolver(Map("db.foo" -> Set("a", "b")))
  def qs(cs: String*)(r: String): QuerySelect[RawName] =
    QuerySelect(Select(
      cs.map(RawColumnName(_)).toList,
      Some(List(RawTableName(r)))
      ))

  "Helper qs" should "generate valid QuerySelects" in {
    qs("a", "b")("db.foo") shouldBe QuerySelect(Select(
                                      List(RawColumnName("a"), RawColumnName("b")),
                                      Some(List(RawTableName("db.foo")))
                                    ))
  }

  "MiniSqlState resolver" should "resolve columns in db in single Select" in {
    val myq: QuerySelect[RawName] = qs("a")("db.foo")
    val actual = resolveQuery(myq).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] =
      QuerySelect(Select(
                    List(ResolvedColumnName("a")),
                    Some(List(ResolvedTableName("db.foo")))
                  ))
    actual shouldBe expectedmyq
  }

  it should "not resolve columns in db in single Select" in {
    val myq: QuerySelect[RawName] = qs("f")("db.foo")
    val actual = resolveQuery(myq).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] =
      QuerySelect(Select(
                    List(UnresolvedColumnName("f")),
                    Some(List(ResolvedTableName("db.foo")))
                  ))
    actual shouldBe expectedmyq
  }

  it should "not resolve columns in db in single Select with relation not in db" in {
    val myq: QuerySelect[RawName] = qs("a")("db.fake")
    val actual = resolveQuery(myq).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] =
      QuerySelect(Select(
                    List(UnresolvedColumnName("a")),
                    Some(List(UnresolvedTableName("db.fake")))
                  ))
    actual shouldBe expectedmyq
  }

  it should "resolve columns in db in QueryWith with single Select in single CTE" in {
    val myq: QuerySelect[RawName] = qs("a")("db.foo")
    val myqw: QueryWith[RawName] = QueryWith(
      List(CTE("myalias", myq)),
      qs("a")("myalias"))
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] =
      QuerySelect(Select(
                    List(ResolvedColumnName("a")),
                    Some(List(ResolvedTableName("db.foo")))
                  ))
    val expectedmyqw: QueryWith[ResolvedName] =
      QueryWith(
        List(CTE("myalias", expectedmyq)),
        QuerySelect(Select(
                      List(ResolvedColumnName("a")),
                      Some(List(ResolvedTableAlias("myalias")))
                    )))

    actual shouldBe expectedmyqw
  }

  it should "not resolve columns in db but not selected in CTE" in {
    val myq: QuerySelect[RawName] = qs("a")("db.foo")
    val myqw: QueryWith[RawName] = QueryWith(
      List(CTE("myalias", myq)),
      qs("b")("myalias"))
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                  List(ResolvedColumnName("a")),
                                                  Some(List(ResolvedTableName("db.foo")))
                                                ))
    val expectedmyqw: QueryWith[ResolvedName] = QueryWith(
                                                  List(CTE("myalias", expectedmyq)),
                                                  QuerySelect(Select(
                                                           List(UnresolvedColumnName("b")),
                                                           Some(List(ResolvedTableAlias("myalias")))
                                                         )))

    actual shouldBe expectedmyqw
  }

  it should "resolve columns in db but and selected in CTE that depends on other CTE" in {
    // WITH
    //   hasA AS (SELECT a FROM db.foo),
    //   secondCte AS (SELECT a FROM hasA)
    // SELECT a FROM secondCte
    val myq: QuerySelect[RawName] = qs("a")("db.foo")
    val grabAfromHasA: QuerySelect[RawName] = qs("a")("hasA")
    val myqw: QueryWith[RawName] = QueryWith(
                                             List(CTE("hasA", myq), CTE("secondCte", grabAfromHasA)),
                                             QuerySelect(Select(
                                                           List(RawColumnName("a")),
                                                           Some(List(RawTableName("secondCte")))
                                                         )))
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                  List(ResolvedColumnName("a")),
                                                  Some(List(ResolvedTableName("db.foo")))
                                                ))
    val expectedgrabAfromHasA: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                            List(ResolvedColumnName("a")),
                                                            Some(List(ResolvedTableAlias("hasA")))
                                                          ))
    val expectedmyqw: QueryWith[ResolvedName] = QueryWith(
                                                  List(CTE("hasA", expectedmyq), CTE("secondCte", expectedgrabAfromHasA)),
                                                  QuerySelect(Select(
                                                           List(ResolvedColumnName("a")),
                                                           Some(List(ResolvedTableAlias("secondCte")))
                                                         )))

    actual shouldBe expectedmyqw
  }

  it should "not resolve columns in db but not selected in CTE that depends on other CTE" in {
    // WITH
    //   hasA AS (SELECT a FROM db.foo),
    //   secondCte AS (SELECT a FROM hasA)
    // SELECT b FROM secondCte  // <-- b should not resolve
    val myq: QuerySelect[RawName] = qs("a")("db.foo")
    val grabAfromHasA: QuerySelect[RawName] = qs("a")("hasA")
    val myqw: QueryWith[RawName] = QueryWith(
                                             List(CTE("hasA", myq), CTE("secondCte", grabAfromHasA)),
                                             QuerySelect(Select(
                                                           List(RawColumnName("b")),
                                                           Some(List(RawTableName("secondCte")))
                                                         )))
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                  List(ResolvedColumnName("a")),
                                                  Some(List(ResolvedTableName("db.foo")))
                                                ))
    val expectedgrabAfromHasA: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                            List(ResolvedColumnName("a")),
                                                            Some(List(ResolvedTableAlias("hasA")))
                                                          ))
    val expectedmyqw: QueryWith[ResolvedName] = QueryWith(
                                                  List(CTE("hasA", expectedmyq), CTE("secondCte", expectedgrabAfromHasA)),
                                                  QuerySelect(Select(
                                                           List(UnresolvedColumnName("b")),
                                                           Some(List(ResolvedTableAlias("secondCte")))
                                                         )))

    actual shouldBe expectedmyqw
  }

  it should "not resolve columns in db but not selected in CTE that depends on other CTE that does select them" in {
    // WITH
    //   hasAandB AS (SELECT a b FROM db.foo),
    //   secondCte AS (SELECT a FROM hasAandB)
    // SELECT b FROM secondCte  // <-- b should not resolve
    val myq: QuerySelect[RawName] = qs("a", "b")("db.foo")
    val grabAfromHasA: QuerySelect[RawName] = qs("a")("hasAandB")
    val myqw: QueryWith[RawName] = QueryWith(
                                             List(CTE("hasAandB", myq), CTE("secondCte", grabAfromHasA)),
                                             QuerySelect(Select(
                                                           List(RawColumnName("b")),
                                                           Some(List(RawTableName("secondCte")))
                                                         )))
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val expectedmyq: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                  List(ResolvedColumnName("a"), ResolvedColumnName("b")),
                                                  Some(List(ResolvedTableName("db.foo")))
                                                ))
    val expectedgrabAfromHasA: QuerySelect[ResolvedName] = QuerySelect(Select(
                                                            List(ResolvedColumnName("a")),
                                                            Some(List(ResolvedTableAlias("hasAandB")))
                                                          ))
    val expectedmyqw: QueryWith[ResolvedName] = QueryWith(
                                                  List(CTE("hasAandB", expectedmyq), CTE("secondCte", expectedgrabAfromHasA)),
                                                  QuerySelect(Select(
                                                           List(UnresolvedColumnName("b")),
                                                           Some(List(ResolvedTableAlias("secondCte")))
                                                         )))

    actual shouldBe expectedmyqw
  }

  it should "resolve ctes inside of ctes" in {
    // WITH
    //   hasAandB AS (SELECT a b FROM db.foo),
    //   bFromInner AS (
    //     WITH
    //       stillAandB AS (SELECT a b FROM hasAandB),
    //       innerCte AS (SELECT b FROM stillAandB)
    //     SELECT b FROM innerCte
    //   )
    // SELECT b FROM bFromInner
    val hasAandB: QuerySelect[RawName] = qs("a", "b")("db.foo")
    val stillAandB: QuerySelect[RawName] = qs("a", "b")("hasAandB")
    val innerCte: QuerySelect[RawName] = qs("b")("stillAandB")
    val bFromInner: QuerySelect[RawName] = qs("b")("innerCte")
    val outer: QuerySelect[RawName] = qs("b")("bFromInner")

    val innerqw: QueryWith[RawName] =
      QueryWith(
        List(CTE("stillAandB", stillAandB), CTE("innerCte", innerCte)),
        bFromInner)
    val myqw: QueryWith[RawName] =
      QueryWith(
        List(CTE("hasAandB", hasAandB), CTE("bFromInner", innerqw)),
        outer)
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val eHasAandB: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("a"), ResolvedColumnName("b")),
                         Some(List(ResolvedTableName("db.foo")))))
    val eStillAandB: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("a"), ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("hasAandB")))))
    val eInnerCte: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("stillAandB")))))
    val eBFromInner: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("innerCte")))))
    val eOuter: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("bFromInner")))))

    val eInnerqw: QueryWith[ResolvedName] =
      QueryWith(
        List(CTE("stillAandB", eStillAandB), CTE("innerCte", eInnerCte)),
        eBFromInner)
    val expectedmyqw: QueryWith[ResolvedName] =
      QueryWith(
        List(CTE("hasAandB", eHasAandB), CTE("bFromInner", eInnerqw)),
        eOuter)

    actual shouldBe expectedmyqw
  }

  it should "not resolve outer columns if not in cte composed of other ctes" in {
    // WITH
    //   hasAandB AS (SELECT a b FROM db.foo),
    //   bFromInner AS (
    //     WITH
    //       stillAandB AS (SELECT a b FROM hasAandB),
    //       innerCte AS (SELECT b FROM stillAandB)
    //     SELECT b FROM innerCte
    //   )
    // SELECT a FROM bFromInner // <-- a should not resolve
    val hasAandB: QuerySelect[RawName] = qs("a", "b")("db.foo")
    val stillAandB: QuerySelect[RawName] = qs("a", "b")("hasAandB")
    val innerCte: QuerySelect[RawName] = qs("b")("stillAandB")
    val bFromInner: QuerySelect[RawName] = qs("b")("innerCte")
    val outer: QuerySelect[RawName] = qs("a")("bFromInner")

    val innerqw: QueryWith[RawName] =
      QueryWith(
        List(CTE("stillAandB", stillAandB), CTE("innerCte", innerCte)),
        bFromInner)
    val myqw: QueryWith[RawName] =
      QueryWith(
        List(CTE("hasAandB", hasAandB), CTE("bFromInner", innerqw)),
        outer)
    val actual = resolveQuery(myqw).runA(initialCatalog).value
    val eHasAandB: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("a"), ResolvedColumnName("b")),
                         Some(List(ResolvedTableName("db.foo")))))
    val eStillAandB: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("a"), ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("hasAandB")))))
    val eInnerCte: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("stillAandB")))))
    val eBFromInner: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(ResolvedColumnName("b")),
                         Some(List(ResolvedTableAlias("innerCte")))))
    val eOuter: QuerySelect[ResolvedName] =
      QuerySelect(Select(List(UnresolvedColumnName("a")),
                         Some(List(ResolvedTableAlias("bFromInner")))))

    val eInnerqw: QueryWith[ResolvedName] =
      QueryWith(
        List(CTE("stillAandB", eStillAandB), CTE("innerCte", eInnerCte)),
        eBFromInner)
    val expectedmyqw: QueryWith[ResolvedName] =
      QueryWith(
        List(CTE("hasAandB", eHasAandB), CTE("bFromInner", eInnerqw)),
        eOuter)

    actual shouldBe expectedmyqw
  }

}
