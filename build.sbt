lazy val ScalaTestVersion  = "3.0.8"
lazy val CatsVersion       = "2.0.0"
lazy val CatsEffectVersion = "2.0.0"
lazy val ScalaCheckVersion = "1.14.0"
lazy val PPrintVersion     = "0.5.5"

lazy val ScalacOptions = Seq(
  scalacOptions ++= Seq(
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-encoding",
    "utf-8", // Specify character encoding used by source files.
    "-explaintypes", // Explain type errors in more detail.
    "-feature", // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked", // Enable additional warnings where generated code depends on assumptions.
    "-Xcheckinit", // Wrap field accessors to throw an exception on uninitialized access.
    "-Xfatal-warnings", // Fail the compilation if there are any warnings.
    "-Xlint:adapted-args", // Warn if an argument list is modified to match the receiver.
    "-Xlint:constant", // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select", // Selecting member of DelayedInit.
    "-Xlint:doc-detached", // A Scaladoc comment appears to be detached from its element.
    "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator", // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-override", // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Xlint:nullary-unit", // Warn when nullary methods return Unit.
    "-Xlint:option-implicit", // Option.apply used implicit view.
    "-Xlint:package-object-classes", // Class or object defined in package object.
    "-Xlint:poly-implicit-overload", // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow", // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align", // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow", // A local type parameter shadows a type already in scope.
    "-Ywarn-dead-code", // Warn when dead code is identified.
    "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
    "-Ywarn-numeric-widen", // Warn when numerics are widened.
    "-Ywarn-value-discard", // Warn when non-Unit expression results are unused.
    "-language:higherKinds",// allow higher kinded types without `import scala.language.higherKinds`
  )
)

lazy val root = (project in file("."))
  .settings(
    ScalacOptions,
    inThisBuild(
      List(
        organization := "ca.valencik",
        scalaVersion := "2.13.0",
        version := "0.1.0-SNAPSHOT",
      )),
    name := "Playground",
    libraryDependencies ++= Seq(
      "org.typelevel"  %% "cats-core"     % CatsVersion,
      "org.typelevel"  %% "cats-effect"   % CatsEffectVersion,
      "com.lihaoyi"    %% "pprint"        % PPrintVersion,
      "org.typelevel"  %% "cats-laws"     % CatsVersion % Test,
      "org.typelevel"  %% "discipline-scalatest"     % "1.0.0-M1" % Test,
      "org.scalatest"  %% "scalatest"     % ScalaTestVersion % Test,
      "org.scalacheck" %% "scalacheck"    % ScalaCheckVersion % Test
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.3"),
    scalacOptions in (Compile, console) --= Seq("-Ywarn-unused:imports", "-Xfatal-warnings"),
    scalacOptions in (Test) --= Seq("-Ywarn-unused:imports")
  )
